import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import  App  from './containers/App'
import { AuthProvider } from "./configuracion/useAuth";
import * as serviceWorker from './serviceWorker'
import { ThemeProvider } from 'styled-components'
import theme from './theme'

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
    <AuthProvider>
      <App />
    </AuthProvider> 
    </BrowserRouter>
  </ThemeProvider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
