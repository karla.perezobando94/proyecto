import React from "react";
import { Formik } from "formik";
import { useHistory, Redirect } from "react-router-dom";
import { useAuth } from "../../configuracion/useAuth";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { Wrapper, H2 } from "./styles";
import validationSchema from "./validationSchema";

export default () => {
  const history = useHistory();
  const auth = useAuth();

  if (auth.isAuthenticated) return <Redirect to="/" />;

  return (
    <Wrapper>
      <H2>Iniciar Sesión</H2>
      <Formik
        initialValues={{ username: "", password: "" }}
        onSubmit={fields => {
          auth.login(fields, () => history.push("/"));
        }}
        validationSchema={validationSchema}
      >
        {({ values, handleChange, handleSubmit, errors }) => (
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label htmlFor="username">Usuario: </Label>
              <br />
              <Input
                id="username"
                name="username"
                type="text"
                value={values.username}
                onChange={handleChange}
              />
              <br />
              {errors.username && (
                <FormText color="danger">{errors.username}</FormText>
              )}
            </FormGroup>
            <br />
            <FormGroup>
              <Label htmlFor="password">Contraseña: </Label>
              <br />
              <Input
                name="password"
                type="password"
                value={values.password}
                onChange={handleChange}
              />
              <br />
              {errors.password && (
                <FormText color="danger">{errors.password}</FormText>
              )}
            </FormGroup>
            <br />
            <Button color="primary">Entrar</Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};
