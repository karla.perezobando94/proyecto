import styled from 'styled-components'

export const H2 = styled.h2`
  color: ${(props) => props.theme.colors.black};
  font-weight: bold;
  font-family: 'Arial';
`

export const Wrapper = styled.form`
  background: ${(props) => props.theme.colors.turquesa};
  margin: auto;
  margin-top: 50px;
  border-radius: 7px;
  padding: 10px;
  width: 280px;
  height: 300px;
  text-align: center;
`
