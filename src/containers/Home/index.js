import React from "react"
import { Link, Switch, Route } from "react-router-dom";
import { useHistory } from "react-router-dom";
import {H2} from "../Login/styles"
import { useAuth } from "../../configuracion/useAuth";

export default () => {
    const auth = useAuth();
    const history = useHistory();
    return (
        <div>
          <ul> 
              <Link to="/"><H2>¡Bienvenid@!</H2></Link><br />
              {auth.isAuthenticated ? (
                <button
                  onClick={() => {
                    auth.logout(() => history.push("/login"));
                  }}
                >
                  Cerrar Sesion
                </button>
              ) : (
                <Link to="/login">Iniciar sesion</Link>
              )}
          </ul>
          <div className="dashboard-content">
        <Switch>
          <Route
            exact
            path="/"
            component={() => (
                <p>
                  Ejemplo de retorno.
                  </p>
                  
            )}
          />
          <Route
            path="/"
            component={() => <Link to="/">Home</Link>}
          />
        </Switch>
      </div>
    </div>
    );
  };