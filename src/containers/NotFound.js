import React from "react"
import { H2 } from "./Login/styles";

const NotFound = () => (
  <H2 variant="title" align="center">
    ¡Página no encontrada!
  </H2>
)

export default NotFound
