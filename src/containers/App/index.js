import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "../Login";
import Home from "../Home"
import NoUrl from '../NotFound'
import ProtectedRouter from "../../configuracion/ProtectedRouter";
import "./styles.css";

export const AuthContext = React.createContext();

export default function App() {
  return (

    <Switch>
      <Route path="/login" component={Login} />
      <ProtectedRouter path="/" Component={Home} />
    </Switch>
  );
}