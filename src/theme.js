import React from 'react'

export default {
  colors: {
    blue: '#5fbfff',
    green: '#00d49d',
    yellow: '#fbffc8',
    red: '#f96737',
    gray: '#9dadbc',
    white: '#dce5db',
    turquesa: '#4ecac0',
    black: '#000'
  }
}
